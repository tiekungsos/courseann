<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'CourseController@index')->name('home');

Route::get('/course/{id}', 'CourseController@course');

Route::resource('admin/posts', 'Admin\\PostsController');

Route::get('/detail/{course}/{page}', 'DetailController@index');


Route::get('/exam/{course}/{type}/{page}', 'ExamController@index');

Route::post('/exam/save/{course}', 'ExamController@saveExam');

Route::get('/seachdata/{id}', 'SeachDataController@index');

Route::resource('user','UserController');