-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2019 at 04:19 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course3`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `score_full` int(11) DEFAULT NULL,
  `page_full` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `child` int(11) DEFAULT NULL,
  `child_in` int(11) DEFAULT NULL,
  `test` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `desc`, `score_full`, `page_full`, `img`, `created_at`, `updated_at`, `child`, `child_in`, `test`) VALUES
(3, 'งานรวบรวมเสียงลูกค้า', 'งานรวบรวมเสียงลูกค้า', NULL, NULL, '/images/PRE_CUSTOMER_COMPLAINT/PRE_CUSTOMER_COMPLAINT-01', NULL, NULL, NULL, 1, 0),
(4, 'งานรับเรื่อง', 'งานรับเรื่อง', NULL, NULL, '/images/PRE_CUSTOMER_COMPLAINT/PRE_CUSTOMER_COMPLAINT-01', NULL, NULL, NULL, 1, 0),
(5, 'หน้าที่ของ CR', 'หน้าที่ของ CR', NULL, NULL, '/images/PRE_CUSTOMER_COMPLAINT/PRE_CUSTOMER_COMPLAINT-01', NULL, NULL, NULL, 1, 0),
(6, 'กระบวนการสำรวจความพึงพอใจลูกค้า', 'กระบวนการสำรวจความพึงพอใจลูกค้า', 20, 55, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-01', NULL, NULL, 3, NULL, 1),
(7, 'ความรู้เบื้องต้นการรวบรวม วิเคราะห์ และใช้ประโยชน์จากเสียงของลูกค้า', 'ความรู้เบื้องต้นการรวบรวม วิเคราะห์ และใช้ประโยชน์จากเสียงของลูกค้า', 20, 11, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-1', NULL, NULL, 3, NULL, 0),
(8, 'Voice of Customers', 'Voice of Customers', NULL, NULL, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-01', NULL, NULL, 3, 1, 1),
(9, 'การบันทึก voc ในระบบ', 'การบันทึก voc ในระบบ', NULL, 19, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-23', NULL, NULL, 8, NULL, 0),
(10, 'การวิเคราะห์ voc', 'การวิเคราะห์ voc', NULL, 6, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-45', NULL, NULL, 8, NULL, 0),
(11, 'HappyTree4U & U-Voice', 'HappyTree4U & U-Voice', NULL, 22, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-02', NULL, NULL, 8, NULL, 0),
(12, 'หลักสูตรลูกค้าสัมพันธ์ระดับต้น', 'หลักสูตรลูกค้าสัมพันธ์ระดับต้น', NULL, 10, '/images/basic_cr1/งานรับเรื่อง01-01', NULL, NULL, 4, 1, 0),
(13, 'PCDR', 'PCDR', NULL, 10, '/images/basic_cr2/งานรับเรื่อง02-01', NULL, NULL, 4, NULL, 0),
(14, 'การเขียนเคสและคู่มือการเขียนเคส', 'การเขียนเคสและคู่มือการเขียนเคส', NULL, 10, '/images/basic_cr3/งานรับเรื่อง03-13', NULL, NULL, 4, 1, 0),
(15, 'เนื้อหา ', 'เนื้อหา  (หลักสูตรลูกค้าสัมพันธ์ระดับต้น)', NULL, 10, '/images/basic_cr1/งานรับเรื่อง01-01', NULL, NULL, 12, 1, 0),
(16, 'กรณีตัวอย่าง', 'กรณีตัวอย่าง (หลักสูตรลูกค้าสัมพันธ์ระดับต้น)', NULL, 12, '/images/basic_cr1/งานรับเรื่อง02-31', NULL, NULL, 12, NULL, 0),
(17, 'ทบทวน', 'ทบทวน (หลักสูตรลูกค้าสัมพันธ์ระดับต้น)', NULL, 4, '/images/basic_cr1/งานรับเรื่อง02-27', NULL, NULL, 12, NULL, 0),
(18, '7 ขั้นตอนการดูแลลูกค้าร้องเรียน', '7 ขั้นตอนการดูแลลูกค้าร้องเรียน', NULL, 42, '/images/basic_cr1/งานรับเรื่อง01-01', NULL, NULL, 15, NULL, 0),
(19, 'Basic CR', 'Basic CR', NULL, 10, '/images/basic_cr1/งานรับเรื่อง02-01', NULL, NULL, 15, NULL, 0),
(20, 'คู่มือการเขียนเคส', 'คู่มือการเขียนเคส', NULL, 16, '/images/basic_cr3/งานรับเรื่อง03-13', NULL, NULL, 14, NULL, 0),
(21, 'การเขียนเคส ', 'การเขียนเคส ', NULL, 12, '/images/basic_cr3/งานรับเรื่อง03-01', NULL, NULL, 14, NULL, 0),
(22, 'ภาระหน้าที่ของ CR', 'ภาระหน้าที่ของ CR', NULL, 27, '/images/cr/12018ภาระหน้าที่CR-00', NULL, NULL, 5, NULL, 0),
(23, 'แผ่นชิบ', 'แผ่นชิบ', NULL, NULL, '/images/PRE_CUSTOMER_COMPLAINT/PRE_CUSTOMER_COMPLAINT-01', NULL, NULL, NULL, 1, 0),
(24, 'เนื้อหาเกี่ยวกับแผ่นชิบ', 'เนื้อหาเกี่ยวกับแผ่นชิบ', NULL, 9, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-1', NULL, NULL, 23, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_user`
--

CREATE TABLE `course_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `score` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail`
--

CREATE TABLE `detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` int(11) NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `detail`
--

INSERT INTO `detail` (`id`, `img`, `page`, `course_id`) VALUES
(136, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-01', 1, 6),
(137, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-02', 2, 6),
(138, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-03', 3, 6),
(139, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-04', 4, 6),
(140, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-05', 5, 6),
(141, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-06', 6, 6),
(142, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-07', 7, 6),
(143, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-08', 8, 6),
(144, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-09', 9, 6),
(145, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-10', 10, 6),
(146, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-11', 11, 6),
(147, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-12', 12, 6),
(148, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-13', 13, 6),
(149, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-14', 14, 6),
(150, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-15', 15, 6),
(151, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-16', 16, 6),
(152, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-17', 17, 6),
(153, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-18', 18, 6),
(154, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-19', 19, 6),
(155, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-20', 20, 6),
(156, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-21', 21, 6),
(157, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-22', 22, 6),
(158, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-23', 23, 6),
(159, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-24', 24, 6),
(160, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-25', 25, 6),
(161, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-26', 26, 6),
(162, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-27', 27, 6),
(163, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-28', 28, 6),
(164, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-29', 29, 6),
(165, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-30', 30, 6),
(166, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-31', 31, 6),
(167, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-32', 32, 6),
(168, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-33', 33, 6),
(169, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-34', 34, 6),
(170, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-35', 35, 6),
(171, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-36', 36, 6),
(172, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-37', 37, 6),
(173, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-38', 38, 6),
(174, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-39', 39, 6),
(175, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-40', 40, 6),
(176, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-41', 41, 6),
(177, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-42', 42, 6),
(178, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-43', 43, 6),
(179, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-44', 44, 6),
(180, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-45', 45, 6),
(181, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-46', 46, 6),
(182, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-47', 47, 6),
(183, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-48', 48, 6),
(184, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-49', 49, 6),
(185, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-50', 50, 6),
(186, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-51', 51, 6),
(187, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-52', 52, 6),
(188, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-53', 53, 6),
(189, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-54', 54, 6),
(190, '/images/customer_ser1/22018CSSurveyforNewCRBasicTraining_update150618-55', 55, 6),
(191, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-1', 1, 7),
(192, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-2', 2, 7),
(193, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-3', 3, 7),
(194, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-4', 4, 7),
(195, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-5', 5, 7),
(196, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-6', 6, 7),
(197, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-7', 7, 7),
(198, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-8', 8, 7),
(199, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-9', 9, 7),
(200, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-10', 10, 7),
(201, '/images/customer_ser2/งานรวบรวมเสียงลูกค้า02-11', 11, 7),
(202, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-01', 1, 11),
(203, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-02', 2, 11),
(204, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-03', 3, 11),
(205, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-04', 4, 11),
(206, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-05', 5, 11),
(207, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-06', 6, 11),
(208, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-07', 7, 11),
(209, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-08', 8, 11),
(210, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-09', 9, 11),
(211, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-10', 10, 11),
(212, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-11', 11, 11),
(213, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-12', 12, 11),
(214, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-13', 13, 11),
(215, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-14', 14, 11),
(216, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-15', 15, 11),
(217, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-16', 16, 11),
(218, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-17', 17, 11),
(219, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-18', 18, 11),
(220, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-19', 19, 11),
(221, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-20', 20, 11),
(222, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-21', 21, 11),
(223, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-22', 22, 11),
(224, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-23', 1, 9),
(225, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-24', 2, 9),
(226, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-25', 3, 9),
(227, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-26', 4, 9),
(228, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-27', 5, 9),
(229, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-28', 6, 9),
(230, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-29', 6, 9),
(231, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-30', 7, 9),
(232, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-31', 8, 9),
(233, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-32', 9, 9),
(234, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-33', 10, 9),
(235, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-34', 11, 9),
(236, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-35', 12, 9),
(237, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-36', 13, 9),
(238, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-37', 14, 9),
(239, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-38', 15, 9),
(240, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-39', 16, 9),
(241, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-40', 17, 9),
(242, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-41', 18, 9),
(243, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-42', 19, 9),
(244, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-43', 20, 9),
(245, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-44', 21, 9),
(246, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-45', 1, 10),
(247, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-46', 2, 10),
(248, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-47', 3, 10),
(249, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-48', 4, 10),
(250, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-49', 5, 10),
(251, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-50', 6, 10),
(252, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-51', 7, 10),
(253, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-52', 8, 10),
(254, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-53', 9, 10),
(255, '/images/customer_ser3/งานรวบรวมเสียงลูกค้า03-54', 10, 10),
(256, '/images/basic_cr1/งานรับเรื่อง01-01', 1, 18),
(257, '/images/basic_cr1/งานรับเรื่อง01-02', 2, 18),
(258, '/images/basic_cr1/งานรับเรื่อง01-03', 3, 18),
(259, '/images/basic_cr1/งานรับเรื่อง01-04', 4, 18),
(260, '/images/basic_cr1/งานรับเรื่อง01-05', 5, 18),
(261, '/images/basic_cr1/งานรับเรื่อง01-06', 6, 18),
(262, '/images/basic_cr1/งานรับเรื่อง01-07', 7, 18),
(263, '/images/basic_cr1/งานรับเรื่อง01-08', 8, 18),
(264, '/images/basic_cr1/งานรับเรื่อง01-09', 9, 18),
(265, '/images/basic_cr1/งานรับเรื่อง01-10', 10, 18),
(266, '/images/basic_cr1/งานรับเรื่อง01-11', 11, 18),
(267, '/images/basic_cr1/งานรับเรื่อง01-12', 12, 18),
(268, '/images/basic_cr1/งานรับเรื่อง01-13', 13, 18),
(269, '/images/basic_cr1/งานรับเรื่อง01-14', 14, 18),
(270, '/images/basic_cr1/งานรับเรื่อง01-15', 15, 18),
(271, '/images/basic_cr1/งานรับเรื่อง01-16', 16, 18),
(272, '/images/basic_cr1/งานรับเรื่อง01-17', 17, 18),
(273, '/images/basic_cr1/งานรับเรื่อง01-18', 18, 18),
(274, '/images/basic_cr1/งานรับเรื่อง01-19', 19, 18),
(275, '/images/basic_cr1/งานรับเรื่อง01-20', 20, 18),
(276, '/images/basic_cr1/งานรับเรื่อง01-21', 21, 18),
(277, '/images/basic_cr1/งานรับเรื่อง01-22', 22, 18),
(278, '/images/basic_cr1/งานรับเรื่อง01-23', 23, 18),
(279, '/images/basic_cr1/งานรับเรื่อง01-24', 24, 18),
(280, '/images/basic_cr1/งานรับเรื่อง01-25', 25, 18),
(281, '/images/basic_cr1/งานรับเรื่อง01-26', 26, 18),
(282, '/images/basic_cr1/งานรับเรื่อง01-27', 27, 18),
(283, '/images/basic_cr1/งานรับเรื่อง01-28', 28, 18),
(284, '/images/basic_cr1/งานรับเรื่อง01-29', 29, 18),
(285, '/images/basic_cr1/งานรับเรื่อง01-30', 30, 18),
(286, '/images/basic_cr1/งานรับเรื่อง01-31', 31, 18),
(287, '/images/basic_cr1/งานรับเรื่อง01-32', 32, 18),
(288, '/images/basic_cr1/งานรับเรื่อง01-33', 33, 18),
(289, '/images/basic_cr1/งานรับเรื่อง01-34', 34, 18),
(290, '/images/basic_cr1/งานรับเรื่อง01-35', 35, 18),
(291, '/images/basic_cr1/งานรับเรื่อง01-36', 36, 18),
(292, '/images/basic_cr1/งานรับเรื่อง01-37', 37, 18),
(293, '/images/basic_cr1/งานรับเรื่อง01-38', 38, 18),
(294, '/images/basic_cr1/งานรับเรื่อง01-39', 39, 18),
(295, '/images/basic_cr1/งานรับเรื่อง01-40', 40, 18),
(296, '/images/basic_cr1/งานรับเรื่อง01-41', 41, 18),
(297, '/images/basic_cr1/งานรับเรื่อง01-42', 42, 18),
(298, '/images/basic_cr1/งานรับเรื่อง02-01', 1, 19),
(299, '/images/basic_cr1/งานรับเรื่อง02-02', 2, 19),
(300, '/images/basic_cr1/งานรับเรื่อง02-03', 3, 19),
(301, '/images/basic_cr1/งานรับเรื่อง02-04', 4, 19),
(302, '/images/basic_cr1/งานรับเรื่อง02-05', 5, 19),
(303, '/images/basic_cr1/งานรับเรื่อง02-06', 6, 19),
(304, '/images/basic_cr1/งานรับเรื่อง02-07', 7, 19),
(305, '/images/basic_cr1/งานรับเรื่อง02-08', 8, 19),
(306, '/images/basic_cr1/งานรับเรื่อง02-09', 9, 19),
(307, '/images/basic_cr2/งานรับเรื่อง02-01', 1, 13),
(308, '/images/basic_cr2/งานรับเรื่อง02-02', 2, 13),
(309, '/images/basic_cr2/งานรับเรื่อง02-03', 3, 13),
(310, '/images/basic_cr2/งานรับเรื่อง02-04', 4, 13),
(311, '/images/basic_cr2/งานรับเรื่อง02-05', 5, 13),
(312, '/images/basic_cr2/งานรับเรื่อง02-06', 6, 13),
(313, '/images/basic_cr2/งานรับเรื่อง02-07', 7, 13),
(314, '/images/basic_cr2/งานรับเรื่อง02-08', 8, 13),
(315, '/images/basic_cr2/งานรับเรื่อง02-09', 9, 13),
(316, '/images/basic_cr2/งานรับเรื่อง02-10', 10, 13),
(317, '/images/basic_cr2/งานรับเรื่อง02-11', 11, 13),
(318, '/images/basic_cr2/งานรับเรื่อง02-12', 12, 13),
(319, '/images/basic_cr2/งานรับเรื่อง02-13', 13, 13),
(320, '/images/basic_cr2/งานรับเรื่อง02-14', 14, 13),
(321, '/images/basic_cr2/งานรับเรื่อง02-15', 15, 13),
(322, '/images/basic_cr2/งานรับเรื่อง02-16', 16, 13),
(323, '/images/basic_cr2/งานรับเรื่อง02-17', 17, 13),
(324, '/images/basic_cr2/งานรับเรื่อง02-18', 18, 13),
(325, '/images/basic_cr2/งานรับเรื่อง02-19', 19, 13),
(326, '/images/basic_cr2/งานรับเรื่อง02-20', 20, 13),
(327, '/images/basic_cr2/งานรับเรื่อง02-21', 21, 13),
(328, '/images/basic_cr2/งานรับเรื่อง02-22', 22, 13),
(329, '/images/basic_cr2/งานรับเรื่อง02-23', 23, 13),
(330, '/images/basic_cr2/งานรับเรื่อง02-24', 24, 13),
(331, '/images/basic_cr2/งานรับเรื่อง02-25', 25, 13),
(332, '/images/basic_cr2/งานรับเรื่อง02-26', 26, 13),
(333, '/images/basic_cr2/งานรับเรื่อง02-27', 27, 13),
(334, '/images/basic_cr2/งานรับเรื่อง02-28', 28, 13),
(335, '/images/basic_cr2/งานรับเรื่อง02-29', 29, 13),
(336, '/images/basic_cr2/งานรับเรื่อง02-30', 30, 13),
(337, '/images/basic_cr2/งานรับเรื่อง02-31', 31, 13),
(338, '/images/basic_cr2/งานรับเรื่อง02-32', 32, 13),
(339, '/images/basic_cr2/งานรับเรื่อง02-33', 33, 13),
(340, '/images/basic_cr2/งานรับเรื่อง02-34', 34, 13),
(341, '/images/basic_cr2/งานรับเรื่อง02-35', 35, 13),
(342, '/images/basic_cr2/งานรับเรื่อง02-36', 36, 13),
(343, '/images/basic_cr2/งานรับเรื่อง02-37', 37, 13),
(344, '/images/basic_cr2/งานรับเรื่อง02-38', 38, 13),
(345, '/images/basic_cr2/งานรับเรื่อง02-39', 39, 13),
(346, '/images/basic_cr1/งานรับเรื่อง02-31', 1, 16),
(347, '/images/basic_cr1/งานรับเรื่อง02-32', 2, 16),
(348, '/images/basic_cr1/งานรับเรื่อง02-33', 3, 16),
(349, '/images/basic_cr1/งานรับเรื่อง02-34', 4, 16),
(350, '/images/basic_cr1/งานรับเรื่อง02-35', 5, 16),
(351, '/images/basic_cr1/งานรับเรื่อง02-36', 6, 16),
(352, '/images/basic_cr1/งานรับเรื่อง02-37', 7, 16),
(353, '/images/basic_cr1/งานรับเรื่อง02-38', 8, 16),
(354, '/images/basic_cr1/งานรับเรื่อง02-39', 9, 16),
(355, '/images/basic_cr1/งานรับเรื่อง02-40', 10, 16),
(356, '/images/basic_cr1/งานรับเรื่อง02-41', 11, 16),
(357, '/images/basic_cr1/งานรับเรื่อง02-42', 12, 16),
(358, '/images/basic_cr1/งานรับเรื่อง02-43', 13, 16),
(359, '/images/basic_cr1/งานรับเรื่อง02-44', 14, 16),
(360, '/images/basic_cr1/งานรับเรื่อง02-27', 1, 17),
(361, '/images/basic_cr1/งานรับเรื่อง02-28', 2, 17),
(362, '/images/basic_cr1/งานรับเรื่อง02-29', 3, 17),
(363, '/images/basic_cr1/งานรับเรื่อง02-30', 4, 17),
(364, '/images/basic_cr3/งานรับเรื่อง03-01', 1, 21),
(365, '/images/basic_cr3/งานรับเรื่อง03-02', 2, 21),
(366, '/images/basic_cr3/งานรับเรื่อง03-03', 3, 21),
(367, '/images/basic_cr3/งานรับเรื่อง03-04', 4, 21),
(368, '/images/basic_cr3/งานรับเรื่อง03-05', 5, 21),
(369, '/images/basic_cr3/งานรับเรื่อง03-06', 6, 21),
(370, '/images/basic_cr3/งานรับเรื่อง03-07', 7, 21),
(371, '/images/basic_cr3/งานรับเรื่อง03-08', 8, 21),
(372, '/images/basic_cr3/งานรับเรื่อง03-09', 9, 21),
(373, '/images/basic_cr3/งานรับเรื่อง03-10', 10, 21),
(374, '/images/basic_cr3/งานรับเรื่อง03-11', 11, 21),
(375, '/images/basic_cr3/งานรับเรื่อง03-12', 12, 21),
(376, '/images/basic_cr3/งานรับเรื่อง03-13', 1, 20),
(377, '/images/basic_cr3/งานรับเรื่อง03-14', 2, 20),
(378, '/images/basic_cr3/งานรับเรื่อง03-15', 3, 20),
(379, '/images/basic_cr3/งานรับเรื่อง03-16', 4, 20),
(380, '/images/basic_cr3/งานรับเรื่อง03-17', 5, 20),
(381, '/images/basic_cr3/งานรับเรื่อง03-18', 6, 20),
(382, '/images/basic_cr3/งานรับเรื่อง03-19', 7, 20),
(383, '/images/basic_cr3/งานรับเรื่อง03-20', 8, 20),
(384, '/images/basic_cr3/งานรับเรื่อง03-21', 9, 20),
(385, '/images/basic_cr3/งานรับเรื่อง03-22', 10, 20),
(386, '/images/basic_cr3/งานรับเรื่อง03-23', 11, 20),
(387, '/images/basic_cr3/งานรับเรื่อง03-24', 12, 20),
(388, '/images/basic_cr3/งานรับเรื่อง03-25', 13, 20),
(389, '/images/basic_cr3/งานรับเรื่อง03-26', 14, 20),
(390, '/images/basic_cr3/งานรับเรื่อง03-27', 15, 20),
(391, '/images/basic_cr3/งานรับเรื่อง03-28', 16, 20),
(392, '/images/cr/12018ภาระหน้าที่CR-00', 1, 22),
(393, '/images/cr/12018ภาระหน้าที่CR-01', 2, 22),
(394, '/images/cr/12018ภาระหน้าที่CR-02', 3, 22),
(395, '/images/cr/12018ภาระหน้าที่CR-03', 4, 22),
(396, '/images/cr/12018ภาระหน้าที่CR-04', 5, 22),
(397, '/images/cr/12018ภาระหน้าที่CR-05', 6, 22),
(398, '/images/cr/12018ภาระหน้าที่CR-06', 7, 22),
(399, '/images/cr/12018ภาระหน้าที่CR-07', 8, 22),
(400, '/images/cr/12018ภาระหน้าที่CR-08', 9, 22),
(401, '/images/cr/12018ภาระหน้าที่CR-09', 10, 22),
(402, '/images/cr/12018ภาระหน้าที่CR-10', 11, 22),
(403, '/images/cr/12018ภาระหน้าที่CR-11', 12, 22),
(404, '/images/cr/12018ภาระหน้าที่CR-12', 13, 22),
(405, '/images/cr/12018ภาระหน้าที่CR-13', 14, 22),
(406, '/images/cr/12018ภาระหน้าที่CR-14', 15, 22),
(407, '/images/cr/12018ภาระหน้าที่CR-15', 16, 22),
(408, '/images/cr/12018ภาระหน้าที่CR-16', 17, 22),
(409, '/images/cr/12018ภาระหน้าที่CR-17', 18, 22),
(410, '/images/cr/12018ภาระหน้าที่CR-18', 19, 22),
(411, '/images/cr/12018ภาระหน้าที่CR-19', 20, 22),
(412, '/images/cr/12018ภาระหน้าที่CR-20', 21, 22),
(413, '/images/cr/12018ภาระหน้าที่CR-21', 22, 22),
(414, '/images/cr/12018ภาระหน้าที่CR-22', 23, 22),
(415, '/images/cr/12018ภาระหน้าที่CR-23', 24, 22),
(416, '/images/cr/12018ภาระหน้าที่CR-24', 25, 22),
(417, '/images/cr/12018ภาระหน้าที่CR-25', 26, 22),
(418, '/images/cr/12018ภาระหน้าที่CR-26', 27, 22),
(419, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-1', 1, 24),
(420, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-2', 2, 24),
(421, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-3', 3, 24),
(422, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-4', 4, 24),
(423, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-5', 5, 24),
(424, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-6', 6, 24),
(425, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-7', 7, 24),
(426, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-8', 8, 24),
(427, '/images/แผ่นชิป/2018_PCDR_บอร์ดแผ่นชิพแม่เหล็กและความหมายCR_WEB[1]-9', 9, 24);

-- --------------------------------------------------------

--
-- Table structure for table `exam_course`
--

CREATE TABLE `exam_course` (
  `id` int(10) NOT NULL,
  `course_id` int(10) NOT NULL,
  `topic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_course`
--

INSERT INTO `exam_course` (`id`, `course_id`, `topic`, `desc`, `type`) VALUES
(1, 6, 'การนำเสียงของลูกค้าไปใช้  ', 'พนักงานเอาใจใส่ดูแลดี มีการนำน้ำมาเสิร์ฟตอนมาถึงโชว์รูม', 1),
(2, 6, 'การนำเสียงของลูกค้าไปใช้  ', 'วันที่ไปที่โชว์รูม ฝนตกหนักมาก พนักงานขายวิ่งถือร่มมารับถึงรถ', 1),
(3, 6, 'การนำเสียงของลูกค้าไปใช้  ', 'S/A ไม่มีการอธิบายค่าใช้จ่ายและรายละเอียดงานซ่อม รู้สึกว่าซ่อมเร็วไป ถามพนักงานก็ไม่ยอมตอบ', 1),
(4, 6, 'การนำเสียงของลูกค้าไปใช้  ', 'ตอนมารับรถใหม่ พนักงานขายไม่อธิบายรายละเอียดใดๆทั้งสิ้น แต่ยังดีมีพนักงานลูกค้าสัมพันธ์เข้ามาแนะนำการนัดหมายนำรถเข้ามารับบริการ', 1),
(5, 6, 'การนำเสียงของลูกค้าไปใช้  ', 'วันที่เอารถไปซ่อม หลานไม่สบาย พนักงานก็หาน้ำอุ่นและยามาให้โดยไม่ต้องขอ', 1),
(6, 8, 'Voice of Customers', '/images/customer_ser3_test/test1', 2),
(7, 8, 'Voice of Customers', '/images/customer_ser3_test/test2', 2),
(8, 8, 'Voice of Customers', '/images/customer_ser3_test/test3', 2),
(9, 8, 'Voice of Customers', '/images/customer_ser3_test/test4', 2),
(10, 8, 'Voice of Customers', '/images/customer_ser3_test/test5', 2),
(11, 8, 'Voice of Customers', '/images/customer_ser3_test/test6', 2);

-- --------------------------------------------------------

--
-- Table structure for table `exam_users`
--

CREATE TABLE `exam_users` (
  `exam_course_id` int(10) NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `side` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_voice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_users`
--

INSERT INTO `exam_users` (`exam_course_id`, `users_id`, `source`, `sales`, `side`, `customer_voice`, `related`) VALUES
(1, 1, 'sdfsd', 'ด้านขาย', 'ด้านบวก', 'gfdgfdgdfg', 'fgddgdfgdf'),
(2, 1, 'dsafdsf', 'ด้านบริการ', 'ด้านบวก', 'fdsfsdfsd', 'sdfdsfsdf'),
(3, 1, 'sdfdsf', 'ด้านขาย', 'ด้านบวก', '0827623720', 'dsfsdfsd'),
(4, 1, 'sdfdsfdsf', 'ด้านขาย', 'ด้านบวก', 'sdfdsfdsf', 'sdfdsfdsf'),
(5, 1, 'fdsfdsf', 'ด้านขาย', 'ด้านบวก', 'dfsdfsdfsd', 'sadfsadfsadfdsaf'),
(6, 1, 'yyyy', NULL, NULL, NULL, NULL),
(7, 1, 'yyyy', NULL, NULL, NULL, NULL),
(8, 1, 'uuuuu', NULL, NULL, NULL, NULL),
(9, 1, 'rrrrrr', NULL, NULL, NULL, NULL),
(10, 1, 'rrrr', NULL, NULL, NULL, NULL),
(11, 1, 'iiiiiii', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_06_162855_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('beeze005@hotmail.com', '$2y$10$0x2Xb12FYKcF8Fu.zQbm8O7TDzpAWeMTj6D3vwu.gv21Pgd7.KWlG', '2019-02-07 06:39:51');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_code` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) DEFAULT '2',
  `delete` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `emp_code`, `name`, `nickname`, `username`, `password`, `remember_token`, `created_at`, `updated_at`, `type`, `delete`) VALUES
(1, 1, 'Thanapon Prunktan', 'tide', 'tiekungsos', '$2y$10$BVtnpQg8JOwClsBZ6WFoC.ptaQBjRkrZPjFblT3OTRXRf5qZEVOXa', 'zGt9rZdTcajQyR313cluxYdwZfSSoaCYDMxz5Y1A2aP9Lp5rmCL11xpkJW2Q', '2019-02-06 08:31:41', '2019-03-22 11:03:07', 1, 0),
(2, 2, 'Thanapon Prunktan', 'tide', 'tiekungsosa', '$2y$10$AiTCRqTn.yqpQWyPxQZAjOd7nhTgN0loHrITNcrbY35rKHjaMH.PW', 'mAH6yhnRfww6ExFql9jYmGNpBvGvKNe8gbfqZed0aybRQUTmxMlpdjtw8cxu', '2019-02-11 02:07:08', '2019-03-22 11:03:09', 2, 1),
(4, 3, 'thanapon prunktan', 'tide', 'tiekungsos', '$2y$10$8VlANw6/DgFOmVl9QmBML.rwTb04WcaEfs4.ClF6Aj3gFHHtvyQRO', 'LjPdavsKfpUmpnn4fQo6jQgGnHAiK12BEs4zTy84V1EtPaVuo4V0g1hwGl9u', '2019-03-13 09:54:10', '2019-03-22 11:03:11', 2, 1),
(5, 4, 'thanapon prunktan', 'tide', 'tiekungsos', '$2y$10$dXKbpUiThpL7xrnlbtrYqee3D3mWGsRRcs2ZQ9uC/vHbS/lhEKgTW', '5owAni72aBHlPD1QXx4fJPFCnOGHlNe2HAhUfIgulfvz5kNjtbNuVoKeKke8', '2019-03-13 10:15:32', '2019-03-25 08:15:51', 2, 1),
(8, 5, 'adasdoo', 'tide', 'asdasd', '$2y$10$yoGRMIbfex4/pEesQaHcF.mNXjrpkUE6Ijqy6q1aDFGHyNvYjWpYC', NULL, '2019-03-13 10:31:13', '2019-03-22 11:03:14', 2, 1),
(9, 6, 'admin', 'admin', 'admin', '$2y$10$4zkhwrL0Y8KDYCnL7zukMeeEb/ggD.T9CpRYHBoOtW4ZmBTt1Faca', NULL, '2019-03-21 11:12:10', '2019-03-22 11:18:11', 2, 0),
(10, 3940, 'วลัยพร', 'นุ่น', 'Walaiporn', '$2y$10$vQ1HNCuUcvNFZSBQpyiabOk9VI7JLX46aEy1tnxuTZjZRaGGIabN6', NULL, '2019-03-22 10:59:40', '2019-03-22 10:59:40', 2, 0),
(11, 4551, 'กรรณิการ์', 'แวน', 'Kannika', '$2y$10$IQhQr/bWgx0FXOCTgpdtBe7gvMDQhqbL4HyWESpLWt3BTMu3L6Fwu', NULL, '2019-03-22 11:00:07', '2019-03-22 11:00:07', 2, 0),
(12, 2159, 'ศิริลักษณ์', 'แพรว', 'Siriluk', '$2y$10$mXSUigone9D.r5zT06YREOrb4BvjwnXrdsq1AaPXOEXXmuIIDGGV2', NULL, '2019-03-22 11:00:34', '2019-03-22 11:00:34', 2, 0),
(13, 2405, 'รุ่งนภา', 'อั๋น', 'Roongnapa', '$2y$10$o.dHZuYIMh2vnP1ULIc/0eWN/zGYd7uBmS1VIZy.Mp8YhBMs5Il.S', 'RX8baMgGG3wpbCDUfe53IblaiCKIb6bjl2glIhCxQEFSMjL0qBQyvGc1oW5y', '2019-03-22 11:00:55', '2019-03-22 11:19:09', 2, 0),
(14, 5555, 'wqeqw', 'qweqw', 'qweqw', '$2y$10$aR/0a.hUcYOGt1EoAtupKeTmKKUXPai0AI0Uap1xv2NQySt0onuAi', NULL, '2019-03-22 11:20:50', '2019-03-22 11:20:53', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `detail_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `detail_id`, `user_id`, `course_id`, `status`) VALUES
(99, 246, 1, 10, 1),
(100, 246, 1, 10, 1),
(101, 202, 1, 11, 1),
(102, 224, 1, 9, 1),
(103, 246, 1, 10, 1),
(104, 246, 1, 10, 1),
(105, 247, 1, 10, 1),
(106, 248, 1, 10, 1),
(107, 248, 1, 10, 1),
(108, 249, 1, 10, 1),
(109, 202, 1, 11, 1),
(110, 203, 1, 11, 1),
(111, 202, 1, 11, 1),
(112, 203, 1, 11, 1),
(113, 204, 1, 11, 1),
(114, 203, 1, 11, 1),
(115, 202, 1, 11, 1),
(116, 204, 1, 11, 1),
(117, 204, 1, 11, 1),
(118, 224, 1, 9, 1),
(119, 204, 1, 11, 1),
(120, 204, 1, 11, 1),
(121, 204, 1, 11, 1),
(122, 204, 1, 11, 1),
(123, 224, 1, 9, 1),
(124, 191, 1, 7, 1),
(125, 204, 1, 11, 1),
(126, 298, 1, 19, 1),
(127, 256, 1, 18, 1),
(128, 298, 1, 19, 1),
(129, 360, 1, 16, 1),
(130, 346, 1, 15, 1),
(131, 307, 1, 13, 1),
(132, 376, 1, 21, 1),
(133, 364, 1, 20, 1),
(134, 346, 1, 15, 1),
(135, 347, 1, 15, 1),
(136, 256, 1, 18, 1),
(137, 392, 1, 22, 1),
(138, 392, 1, 22, 1),
(139, 392, 1, 22, 1),
(140, 393, 1, 22, 1),
(141, 394, 1, 22, 1),
(142, 395, 1, 22, 1),
(143, 224, 1, 9, 1),
(144, 204, 1, 11, 1),
(145, 204, 1, 11, 1),
(146, 191, 1, 7, 1),
(147, 298, 1, 19, 1),
(148, 299, 1, 19, 1),
(149, 300, 1, 19, 1),
(150, 301, 1, 19, 1),
(151, 136, 1, 6, 1),
(152, 136, 1, 6, 1),
(153, 307, 1, 13, 1),
(154, 395, 1, 22, 1),
(155, 136, 1, 6, 1),
(156, 191, 1, 7, 1),
(157, 192, 1, 7, 1),
(158, 193, 1, 7, 1),
(159, 194, 1, 7, 1),
(160, 195, 1, 7, 1),
(161, 196, 1, 7, 1),
(162, 197, 1, 7, 1),
(163, 198, 1, 7, 1),
(164, 199, 1, 7, 1),
(165, 200, 1, 7, 1),
(166, 201, 1, 7, 1),
(167, 200, 1, 7, 1),
(168, 201, 1, 7, 1),
(169, 201, 1, 7, 1),
(170, 201, 1, 7, 1),
(171, 136, 1, 6, 1),
(172, 224, 1, 9, 1),
(173, 225, 1, 9, 1),
(174, 226, 1, 9, 1),
(175, 227, 1, 9, 1),
(176, 228, 1, 9, 1),
(177, 229, 1, 9, 1),
(178, 231, 1, 9, 1),
(179, 232, 1, 9, 1),
(180, 233, 1, 9, 1),
(181, 234, 1, 9, 1),
(182, 235, 1, 9, 1),
(183, 236, 1, 9, 1),
(184, 237, 1, 9, 1),
(185, 238, 1, 9, 1),
(186, 239, 1, 9, 1),
(187, 240, 1, 9, 1),
(188, 241, 1, 9, 1),
(189, 242, 1, 9, 1),
(190, 243, 1, 9, 1),
(191, 249, 1, 10, 1),
(192, 250, 1, 10, 1),
(193, 251, 1, 10, 1),
(194, 204, 1, 11, 1),
(195, 205, 1, 11, 1),
(196, 206, 1, 11, 1),
(197, 207, 1, 11, 1),
(198, 208, 1, 11, 1),
(199, 209, 1, 11, 1),
(200, 210, 1, 11, 1),
(201, 211, 1, 11, 1),
(202, 212, 1, 11, 1),
(203, 213, 1, 11, 1),
(204, 214, 1, 11, 1),
(205, 215, 1, 11, 1),
(206, 216, 1, 11, 1),
(207, 217, 1, 11, 1),
(208, 218, 1, 11, 1),
(209, 219, 1, 11, 1),
(210, 220, 1, 11, 1),
(211, 221, 1, 11, 1),
(212, 222, 1, 11, 1),
(213, 223, 1, 11, 1),
(214, 136, 1, 6, 1),
(215, 137, 1, 6, 1),
(216, 138, 1, 6, 1),
(217, 139, 1, 6, 1),
(218, 140, 1, 6, 1),
(219, 141, 1, 6, 1),
(220, 142, 1, 6, 1),
(221, 143, 1, 6, 1),
(222, 144, 1, 6, 1),
(223, 145, 1, 6, 1),
(224, 146, 1, 6, 1),
(225, 147, 1, 6, 1),
(226, 148, 1, 6, 1),
(227, 149, 1, 6, 1),
(228, 150, 1, 6, 1),
(229, 151, 1, 6, 1),
(230, 152, 1, 6, 1),
(231, 153, 1, 6, 1),
(232, 154, 1, 6, 1),
(233, 155, 1, 6, 1),
(234, 156, 1, 6, 1),
(235, 157, 1, 6, 1),
(236, 158, 1, 6, 1),
(237, 159, 1, 6, 1),
(238, 160, 1, 6, 1),
(239, 161, 1, 6, 1),
(240, 162, 1, 6, 1),
(241, 163, 1, 6, 1),
(242, 164, 1, 6, 1),
(243, 165, 1, 6, 1),
(244, 166, 1, 6, 1),
(245, 167, 1, 6, 1),
(246, 168, 1, 6, 1),
(247, 169, 1, 6, 1),
(248, 170, 1, 6, 1),
(249, 171, 1, 6, 1),
(250, 172, 1, 6, 1),
(251, 173, 1, 6, 1),
(252, 174, 1, 6, 1),
(253, 175, 1, 6, 1),
(254, 176, 1, 6, 1),
(255, 177, 1, 6, 1),
(256, 178, 1, 6, 1),
(257, 179, 1, 6, 1),
(258, 180, 1, 6, 1),
(259, 181, 1, 6, 1),
(260, 182, 1, 6, 1),
(261, 183, 1, 6, 1),
(262, 184, 1, 6, 1),
(263, 185, 1, 6, 1),
(264, 186, 1, 6, 1),
(265, 187, 1, 6, 1),
(266, 188, 1, 6, 1),
(267, 189, 1, 6, 1),
(268, 190, 1, 6, 1),
(269, 190, 1, 6, 1),
(270, 190, 1, 6, 1),
(271, 190, 1, 6, 1),
(272, 190, 1, 6, 1),
(273, 190, 1, 6, 1),
(274, 190, 1, 6, 1),
(275, 190, 1, 6, 1),
(276, 190, 1, 6, 1),
(277, 190, 1, 6, 1),
(278, 307, 1, 13, 1),
(279, 308, 1, 13, 1),
(280, 136, 4, 6, 1),
(281, 190, 1, 6, 1),
(282, 190, 1, 6, 1),
(283, 190, 1, 6, 1),
(284, 201, 1, 7, 1),
(285, 395, 1, 22, 1),
(286, 190, 1, 6, 1),
(287, 395, 1, 22, 1),
(288, 256, 1, 18, 1),
(289, 301, 1, 19, 1),
(290, 308, 1, 13, 1),
(291, 360, 1, 16, 1),
(292, 361, 1, 16, 1),
(293, 362, 1, 16, 1),
(294, 395, 1, 22, 1),
(295, 201, 1, 7, 1),
(296, 223, 1, 11, 1),
(297, 222, 1, 11, 1),
(298, 221, 1, 11, 1),
(299, 222, 1, 11, 1),
(300, 223, 1, 11, 1),
(301, 395, 1, 22, 1),
(302, 376, 1, 20, 1),
(326, 395, 1, 22, 1),
(327, 396, 1, 22, 1),
(328, 396, 1, 22, 1),
(330, 348, 1, 16, 1),
(331, 301, 1, 19, 1),
(332, 300, 1, 19, 1),
(333, 299, 1, 19, 1),
(334, 298, 1, 19, 1),
(335, 308, 1, 13, 1),
(336, 201, 1, 7, 1),
(337, 308, 1, 13, 1),
(338, 419, 1, 24, 1),
(339, 419, 1, 24, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_user`
--
ALTER TABLE `course_user`
  ADD PRIMARY KEY (`user_id`,`course_id`),
  ADD KEY `fk_users_has_course_course1` (`course_id`);

--
-- Indexes for table `detail`
--
ALTER TABLE `detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detail_course1` (`course_id`);

--
-- Indexes for table `exam_course`
--
ALTER TABLE `exam_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_users`
--
ALTER TABLE `exam_users`
  ADD PRIMARY KEY (`exam_course_id`,`users_id`),
  ADD KEY `fk_exam_couse_has_users_users1` (`users_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `detail`
--
ALTER TABLE `detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=428;

--
-- AUTO_INCREMENT for table `exam_course`
--
ALTER TABLE `exam_course`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_user`
--
ALTER TABLE `course_user`
  ADD CONSTRAINT `fk_users_has_course_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_course_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `detail`
--
ALTER TABLE `detail`
  ADD CONSTRAINT `fk_detail_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `exam_users`
--
ALTER TABLE `exam_users`
  ADD CONSTRAINT `fk_exam_couse_has_users_exam_couse1` FOREIGN KEY (`exam_course_id`) REFERENCES `exam_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exam_couse_has_users_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
