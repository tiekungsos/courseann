@extends('layouts.app')

@section('content')
<div class="container">
        <div class="container">
            <div class="row">   
                <div class="col-md-12">
                        {{-- <a href="#" class="btn btn-outline-primary"><i class="fas fa-home"></i> หน้าแรก</a> --}}
                <a href="{{$backToCourse}}" class="btn btn-outline-danger"><i class="fas fa-ban"></i> ยกเลิกการทำแบบทดสอบ</a>
                        <br><br>
                  @foreach($exam as $key => $exam)

                  @if($exam->type == 1)
                    <div class="card mb-3" align="left">
                        <div class="card-header" align="center">
                                    <strong><h5>Case : {{$exam->desc}}</h5></strong>
                        </div>
                        <div class="card-body">
                            <form class="was-validated" method="POST" action="/exam/save/{{$exam->id}}">
                              @csrf                
                                <div class="form-group">
                                  <strong><label for="exampleFormControlInput1">ที่มา</label></strong>
                                  <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="กรุณากรอกข้อมูล" name="source" required>
                                  <div class="invalid-feedback">
                                      กรุณากรอกที่มา
                                    </div>
                                </div>
                                <div class="form-group">
                                  <strong><label for="exampleFormControlSelect1">ด้านขาย/ด้านบริการ</label></strong>
                                  <select class="form-control" id="exampleFormControlSelect1" name="sales" required>
                                    <option value="">กรุณาเลือก</option>
                                    <option value="ด้านขาย">ด้านขาย</option>
                                    <option value="ด้านบริการ">ด้านบริการ</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <strong><label for="exampleFormControlSelect2">เหตุผล</label></strong>
                                  <strong><label for="exampleFormControlSelect1">ด้านขาย/ด้านบริการ</label></strong>
                                  <select class="form-control" id="exampleFormControlSelect1" name="side" required>
                                    <option value="">กรุณาเลือก</option>
                                    <option value="ด้านบวก">ด้านบวก(+)</option>
                                    <option value="ด้านลบ">ด้านลบ (-)</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                    <strong><label for="exampleFormControlInput1">แนวทางการนำเสียงลูกค้าไปใช้</label></strong>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="กรุณากรอกข้อมูล" name="customer_voice" required>
                                    <div class="invalid-feedback">
                                        กรุณากรอกแนวทางการนำเสียงลูกค้าไปใช้
                                      </div>
                                </div>
                                <div class="form-group">
                                    <strong><label for="exampleFormControlInput1">แนวทางการนำเสียงลูกค้าไปใช้</label></strong>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="กรุณากรอกข้อมูล" name="related" required>
                                    <div class="invalid-feedback">
                                        กรุณากรอกแนวทางการนำเสียงลูกค้าไปใช้
                                      </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-outline-success" type="submit">ทำข้อต่อไป</button>
                                </div>
                               
                            </form>

                            {{-- <div class="text-center">
                                @if($exam->fine == false)
                                  <a href="{{$nextCourse}}" class="btn btn-outline-success" >ดูเฉลย</a>
                                @endif
                            </div> --}}
                             
                        </div>
                     </div>
                     @else
                      <div class="card mb-3   " align="center">
                        <div class="card-header">
                          
                                    <strong>{{$exam->topic}} ข้อที่ {{$exam->id - 5}}</strong>
                        </div>
                    <img src="{{$exam->img}}" class="img-fluid" alt="Responsive image">
                    {{-- <img xlink:href="{{$courseDetail->img}}"  class="img-fluid" alt="Image"/> --}}
                        <div class="card-body" >
                          <form class="was-validated" method="POST" action="/exam/save/{{$exam->id}}">
                            @csrf                
                              <div class="form-group">
                                <strong><label for="exampleFormControlInput1">ตอบคำถาม</label></strong>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="กรุณากรอกข้อมูล" name="source" required>
                                <div class="invalid-feedback">
                                    กรุณากรอกคำตอบ
                                  </div>
                              </div>
  
                              <div class="text-center">
                                  <button class="btn btn-outline-success" type="submit">ทำข้อต่อไป</button>
                              </div>
                             
                          </form>
                        </div>
                     </div>

                     @endif
                    @endforeach

                    

                </div>
                
            </div>
        </div>
</div>
@include('seach')
@endsection

