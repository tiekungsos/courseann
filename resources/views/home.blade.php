@extends('layouts.app')

@section('content')

<div class="container">
        @yield('content')
    <div class="album py-5 bg-light">
        <div class="container">
          @if ($message = Session::get('success'))
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
        
          <div class="row">
        @foreach ($course as $course)
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" 
                    preserveAspectRatio="xMidYMid slice" focusable="false" role="img" 
                    aria-label="Placeholder: Thumbnail">
                    <title>Placeholder</title>
                    {{-- <rect fill="#55595c" width="100%" height="100%"></rect> --}}
                    @if($course->img != null)
                      <image xlink:href="{{$course->img}}" width="100%" height="225" alt="Image"/>
                    @endif
                </svg>
                <div class="card-body">
                  <p class="card-text">{{ $course->name }}</p>
                  {{-- <div class="progress">
                  <div class="progress-bar bg-success" role="progressbar" style="{{$course->currentString}}" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$course->currentPage}}%</div>
                    </div> --}}
                    <br>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <a href="{{$course->url}}" class="btn btn-sm btn-outline-secondary"> เข้าไปยังบทเรียน</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
</div>
@include('seach')
@endsection



