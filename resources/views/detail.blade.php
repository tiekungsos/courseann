@extends('layouts.app')

@section('content')
<div class="container">
        <div class="container">
            <div class="row">
                   
                <div class="col-md-12">
                        {{-- <a href="#" class="btn btn-outline-primary"><i class="fas fa-home"></i> หน้าแรก</a> --}}
                <a href="{{$backToCourse}}" class="btn btn-outline-danger"><i class="fas fa-ban"></i> ยกเลิกการเรียน</a>
                        <br><br>
                    <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" 
                    aria-valuenow="75" 
                    aria-valuemin="0" aria-valuemax="100" style="{{$percent}}" >  {{$courseDetail->page}}/{{$courseDetail->course->page_full}} หน้า</div>
                    </div>
                    <div class="card mb-3   " align="center">
                        <div class="card-header">
                                    <strong>{{$courseDetail->course->name}}</strong>
                        </div>
                    <img src="{{$courseDetail->img}}" class="img-fluid" alt="Responsive image">
                    {{-- <img xlink:href="{{$courseDetail->img}}"  class="img-fluid" alt="Image"/> --}}
                        <div class="card-body" >
                            @if($courseDetail->page > 1)
                            <a href="{{$previous}}" class="btn btn-outline-primary"><i class="fas fa-arrow-left"></i> ย้อนกลับ</a>
                            @endif
                             <strong>{{$courseDetail->page}}/{{$courseDetail->course->page_full}}</strong>

                            @if($courseDetail->page != $courseDetail->course->page_full)
                                <a href="{{$nextpage}}" class="btn btn-outline-primary">หน้าต่อไป <i class="fas fa-arrow-right"></i></a>
                            @else
                                <a href="{{$backToCourse}}" class="btn btn-outline-success">เรียนเสร็จแล้ว คลิกเพื่อไปยังหน้าแรก <i class="fas fa-file-signature"></i></a>
                            @endif
                        </div>
                     </div>
                </div>
            </div>
        </div>
</div>
@include('seach')
@endsection

