@extends('layouts.app')

@section('content')

<div class="container">
        @yield('content')
       
        {{--  <nav class="navbar navbar-expand-lg navbar-light bg-info">
            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="color: ivory;">
              หน้า : &nbsp;
              <ul class="navbar-nav mr-auto">
                @foreach($pagination as $page)
                <li class="nav-item active" style="color: ivory;">
                  {{$page}}
                  @if(($pagination)->last() != $page)
                  > &nbsp;
                  @endif
                </li>
                @endforeach
              </ul>
            </div>
          </nav>  --}}

          <div class="container">
            <div class="row">
              <div class="col-sm-12" style="background-color: #e02128;color: ivory;">
                หน้า : &nbsp;
                  @foreach($pagination as $page)
                    {{$page}}
                    @if(($pagination)->last() != $page)
                    > &nbsp;
                    @endif
                  @endforeach
              </div>
            </div>
          </div>

          <br>
          <a href="{{$backToCourse}}" class="btn btn-outline-danger"><i class="fas fa-chevron-circle-left"></i> ย้อนกลับ</a>
          <br><br>      
    <div class="album py-5 bg-light">
        <div class="container">
    
        
          <div class="row">
        @foreach ($course as $course)
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" 
                    preserveAspectRatio="xMidYMid slice" focusable="false" role="img" 
                    aria-label="Placeholder: Thumbnail">
                    <title>Placeholder</title>
                    {{-- <rect fill="#55595c" width="100%" height="100%"></rect> --}}
                    @if($course->img != null)
                      <image xlink:href="{{$course->img}}" width="100%" height="225" alt="Image"/>
                    @endif
                </svg>
                <div class="card-body">
                  <p class="card-text">{{ $course->name }}</p>
                  @if($course->child_in == null)
                  <div class="progress">
                  <div class="progress-bar bg-success" role="progressbar" style="{{$course->currentString}}" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$course->currentPage}}%</div>
                    </div>
                    @else
                    <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width:100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">ยังมีบทเรียนย่อยรอคุณอยู่ {{$course->currentPage}} บทเรียน</div>
                    </div>
                    @endif
                    <br>
                
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <a href="{{$course->url}}" class="btn btn-sm btn-outline-secondary"> เข้าเรียน</a>
                      
                      @if($course->testShow && $course->test && $course->Testurl != null)
                        <a href="{{$course->Testurl}}" class="btn btn-sm btn-outline-secondary">ทำแบบทดสอบ</a>
                      {{-- @elseif($course->testShow && $course->test && $course->Testurl != null && $course->Test == null)
                        <a href="{{$course->Testurl}}" class="btn btn-sm btn-outline-secondary" >ทำแบบทดสอบเสร็จเรียบร้อยแล้ว</a> --}}
                      @endif
                    </div>
                    <br>
                    {{-- @if($course->test)
                    <small class="text-muted">
                        สำเร็จได้คะแนน 20
                        @foreach ($course->user as $users)
                        {{$users->pivot->score}} 
                        @endforeach
                    </small>
                    @endif --}}

                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>

        </div>
      </div>
</div>
@include('seach')
@endsection

