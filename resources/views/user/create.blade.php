@extends('layouts.app')
@section('content')
<div class="container">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New user</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('user.index') }}">กลับสู่หน้าจัดการผู้ใช้งาน</a>
        </div>
    </div>
</div>
   <br>
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> มีบางอย่างผิดพลาดโปรดตรวจสอบ<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('user.store') }}" method="POST">
    @csrf
  
     <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>รหัสพนักงาน:</strong>
                <input type="number" name="emp_code" class="form-control" placeholder="รหัสพนักงาน">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ชื่อจริง:</strong>
                <input type="text" name="name" class="form-control" placeholder="ชื่อจริง">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>ชื่อเข้าใช้งาน:</strong>
                    <input type="text" name="username" class="form-control" placeholder="ชื่อเข้าใช้งาน (กรุณากรอกเป็นภาษาอังกฤษ)">
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>ชื่อเล่น:</strong>
                    <input type="text" name="nickname" class="form-control" placeholder="ชื่อเล่น">
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>สิทธิ์การเข้าถึง : </strong>
                    <label class="radio-inline">
                            <input type="radio" value="1" name="type" >Admin
                          </label>
                          <label class="radio-inline">
                            <input type="radio" value="2" name="type" checked>User
                          </label>
                </div>
        </div>
  


        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>รหัสผ่าน:</strong>
                    <input type="password" name="password" class="form-control" placeholder="รหัสผ่าน (กรุณาระบุอย่างน้อย 6 ตัว)">
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">ยืนยัน</button>
        </div>
    </div>
   
</form>
</div>
@endsection