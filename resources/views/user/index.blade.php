@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>จัดการพนักงาน</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('user.create') }}">เพิ่มพนักงาน</a>
                
            </div>
        </div>
    </div>
    <br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>หมายเลข</th>
            <th>ชื่อจริง</th>
            <th>ชื่อเข้าสู่ระบบ</th>
            <th width="280px">ชื่อเล่น</th>
            <th width="380px"></th>
        </tr>
        @foreach ($user as $user)
        <tr>
            <td>{{ $user->emp_code }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->nickname }}</td>

            <td>
                <form action="{{ route('user.destroy',$user->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('user.show',$user->id) }}">แสดงรายละเอียด</a>

 
    
                    <a class="btn btn-primary" href="{{ route('user.edit',$user->id) }}">แก้ไข</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">ลบผู้ใช้งาน</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
</div>
      
@endsection