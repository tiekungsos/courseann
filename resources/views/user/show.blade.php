@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> แสดงรายละเอียดพนักงาน</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('user.index') }}"> กลับสู่หน้าจัดการผู้ใช้งาน</a>
            </div>
        </div>
    </div>
   <br>
   <div class="card">
        <div class="card-body">
            <div class="row"> 
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6" align="right"><h5><strong>รหัสพนักงาน:</strong></h5></div>
                            <div class="col-sm-6"> <h5>{{ $user->emp_code }}</h5></div>
                        </div>
                    </div>
                </div>    
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6" align="right"><h5><strong>ชื่อจริง:</strong></h5></div>
                            <div class="col-sm-6"> <h5>{{ $user->name }}</h5></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6" align="right"><h5><strong>ชื่อเข้าใช้งาน:</strong></h5></div>
                            <div class="col-sm-6" > <h5>{{ $user->username }}</h5></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6" align="right"><h5><strong>ชื่อเล่น:</strong></h5></div>
                                <div class="col-sm-6"> <h5>  {{ $user->nickname }}</h5></div>
                            </div>
                        </div>
                </div>
            </div>
        </div> 
    </div>
    <br>
    <table class="table table-bordered">
        <thead>
          <tr  align="center">
            <th>#</th>
            <th>ชื่อบทเรียน</th>
            <th>ติดตามบทเรียน</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($course as $key => $course)
                <tr>
                    <td style="width:5%">{{$key + 1}}</td>
                    <td style="width:35%">{{$course->desc}}</td>
                    <td style="width:60%">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="{{$course->currentString}}"  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$course->currentPage}}%</div>
                        </div>
                        <br>
                        @if($course->currentPage == 100 && $course->test ||
                            $course->currentPage == 100 && $course->child == 8)
                            <button class="btn  btn-success" role="button" data-toggle="modal" data-target="#modal-view-{{ $course->id }}"> คลิกเพื่อดูผลการทดสอบ </button>
                        @endif
                        
                    </td>

                  </tr>

                       <!-- The Modal -->
         <div class="modal fade" id="modal-view-{{ $course->id }}" tabIndex="-1">
                <div class="modal-dialog modal-lg">
                <div class="modal-content">
                
                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    
                    <div class="modal-body">
                    
                        @if($course->examType == 1)
                        <div class="card-header" align="center">
                             <strong><h5>คำตอบของ {{$user->name}}</h5></strong>
                        </div>
                        <div class="card-body"> 
                            <div class="row">
                                <div class="col-sm-4" >เสียงของลูกค้า</div>
                                <div class="col-sm-2" >ที่มา</div>
                                <div class="col-sm-1" >ด้านขาย/ด้านบริการ</div>
                                <div class="col-sm-1" >ด้านบวก(+) ด้านลบ (-)</div>
                                <div class="col-sm-2" >แนวทางการนำเสียงลูกค้าไปใช้</div>
                                <div class="col-sm-2" >ฝ่ายที่เกี่ยวข้อง</div>
                            </div>
                            @foreach($course->exam as $key => $exams)
                            <div class="row">
                                    <div class="col-sm-4" >{{$key + 1}} {{$exams->desc}}</div>
                                    <div class="col-sm-2" >{{$exams->source}}</div>
                                    <div class="col-sm-1" >{{$exams->sales}}</div>
                                    <div class="col-sm-1" >{{$exams->side}}</div>
                                    <div class="col-sm-2" >{{$exams->customer_voice}}</div>
                                    <div class="col-sm-2" >{{$exams->related}}</div>
                            </div>
                            @endforeach
                        </div>  
                        @else
                        @foreach ($course->exam as $exams)
                        
                       <div class="card mb-3" align="left">
                                    <div class="card-header" align="center">
                                    <strong><h5>เฉลยคำตอบข้อที่ {{$exams->id - 5}}</h5></strong>
                                    </div>
                                    <div class="card-body"> 
                                        <img src="{{$exams->result}}" class="img-fluid" alt="Responsive image">
                                    </div>
                                </div>
            
                                <div class="card mb-3" align="center">
                                        <div class="card-header" >
                                                    <strong><h5>คำตอบของคุณ</h5></strong>
                                        </div>
                                        <div class="card-body"> 
                                            <h2>{{$exams->source}}</h2>
                                        </div>
                                    </div>
                             @endforeach
                        @endif
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
                </div>
            </div>
            @endforeach
        </tbody>
      </table>
</div>
@endsection