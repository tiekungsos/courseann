@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>แก้ไขผู้ใช้งาน</h2>
            </div>
            @if(Auth::user()->type == 1 )
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('user.index') }}"> กลับสู่หน้าจัดการผู้ใช้งาน</a>
            </div>
            @endif
        </div>
    </div>
   <br>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> มีบางอย่างผิดพลาดโปรดตรวจสอบ.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('user.update',$user->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>รหัสพนักงาน:</strong>
                            <input type="number"  value="{{ $user->emp_code }}" name="emp_code" class="form-control" placeholder="รหัสพนักงาน" disabled>
                        </div>
                    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>ชื่อจริง:</strong>
                        <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="ชื่อจริง">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>ชื่อเข้าใช้งาน:</strong>
                            <input type="text" name="username" value="{{ $user->username }}" class="form-control" placeholder="ชื่อเข้าใช้งาน (กรุณากรอกเป็นภาษาอังกฤษ)">
                        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>ชื่อเล่น:</strong>
                            <input type="text" name="nickname"  value="{{ $user->nickname }}" class="form-control" placeholder="ชื่อเล่น">
                        </div>
                </div>
                @if(Auth::user()->type == 1 )
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>สิทธิ์การเข้าถึง : </strong>
                            <label class="radio-inline">
                                    <input type="radio" value="1" name="type" {{ $user->type == '1' ? 'checked' : ''}} >Admin
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" value="2" name="type"  {{ $user->type == '2' ? 'checked' : ''}} >User
                                  </label>
                        </div>
                </div>
                 @endif
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>รหัสผ่าน:</strong>
                            <input type="password" name="password"  class="form-control" placeholder="ถ้าไม่เปลี่ยนรหัสผ่านไม่ต้องกรอก">
                        </div>
                </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
            </div>
        </div>
   
    </form>
</div>
@endsection