
        <button class="open-button" onclick="openForm()">ค้นหาบทเรียน</button>

        <div class="chat-popup" id="myForm">
            <br>
        <form action="/action_page.php" class="form-container">
            <h1>ค้นหาบทเรียน</h1>
            @foreach ($courseBox as $courseBox)  
            <ul id="treeview" style="list-style-type:circle;">
                <li data-expanded="true"> <strong>{{$courseBox['name']}}</strong>
                  @if($courseBox['child'])  
                    @foreach ($courseBox['child'] as $course)  
                        <ul class="uiSeach">
                            <li>
                                @if($course['child_in'] == null)
                                    <a href="{{ url('/seachdata/'.$course['id']) }}" >{{$course['name']}}</a> 
                                @else 
                                    <strong>{{$course['name']}}</strong>
                                @endif
                                @if($course['child_in'])
                                    @foreach ($course['childin'] as $coursein)  
                                        <ul class="uiSeach">
                                            <li>
                                                @if($coursein['child_in'] == null)
                                                    <a href="{{ url('/seachdata/'.$coursein['id']) }}" >{{$coursein['name']}}</a> 
                                                @else 
                                                    <strong>{{$coursein['name']}}</strong>
                                                @endif
                                                @if($coursein['child_in'])
                                                    @foreach ($coursein['childinin'] as $coursein2)  
                                                        <ul class="uiSeach">
                                                            <li>
                                                                @if($coursein2['child_in'] == null)
                                                                    <a href="{{ url('/seachdata/'.$coursein2['id']) }}" >{{$coursein2['name']}}</a> 
                                                                @else 
                                                                    <strong>{{$coursein2['name']}}</strong>
                                                                @endif 
                                                                
                                                            </li>
                                                        </ul>
                                                    @endforeach   
                                                @endif 
                                            </li>
                                        </ul>
                                    @endforeach   
                                @endif        
                            </li>
                        </ul>
                    @endforeach
                @endif        
                </li>
            </ul>
            @endforeach
            <br>
            {{--  <label for="msg"><b>กรอกข้อมูลที่ต้องการค้นหา</b></label>  --}}
            {{--  <textarea placeholder="พิมพ์ข้อมูลที่ต้องการ" name="msg" required></textarea>  --}}

            {{--  <button type="submit" class="btn">ค้นหา</button>  --}}
            <button type="button" class="btn cancel" onclick="closeForm()">ยกเลิก</button>
        </form>
        </div>


        <script>
        function openForm() {
            document.getElementById("myForm").style.display = "block";
        }
        
        function closeForm() {
            document.getElementById("myForm").style.display = "none";
        }
        </script>
