@extends('layouts.app')

@section('content')
<div class="container">
        <div class="container">
            <div class="row">   
                <div class="col-md-12">
                        {{-- <a href="#" class="btn btn-outline-primary"><i class="fas fa-home"></i> หน้าแรก</a> --}}
                <a href="{{$backToCourse}}" class="btn btn-outline-danger"><i class="fas fa-ban"></i> กลับไปยังหน้าบทเรียน</a>
                        <br><br>
                @if($type == 1)
                    <div class="card mb-3" align="left">
                        <div class="card-header" align="center">
                                    <strong><h5>เฉลยคำตอบ</h5></strong>
                        </div>
                        <div class="card-body"> 
                            <img src="{{$image}}" class="img-fluid" alt="Responsive image">
                        </div>
                     </div>

                     <div class="card mb-3" align="left">
                            <div class="card-header" align="center">
                                        <strong><h5>คำตอบของคุณ</h5></strong>
                            </div>
                            <div class="card-body"> 
                                <table class="table">
                                    <thead>
                                        <tr>
                                                <th scope="col" style="text-align: center;">เสียงของลูกค้า</th>
                                                <th scope="col">ที่มา</th>
                                                <th scope="col">ด้านขาย/ด้านบริการ</th>
                                                <th scope="col">ด้านบวก(+) ด้านลบ (-)</th>
                                                <th scope="col">แนวทางการนำเสียงลูกค้าไปใช้</th>
                                                <th scope="col">ฝ่ายที่เกี่ยวข้อง</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($exam as $exams)
                                            <tr>
                                                <th scope="row" style="width: 25%" >{{$exams->desc}}</th>
                                                <td style="width: 20%">{{$exams->source}}}</td>
                                                <td style="width: 10%">{{$exams->sales}}</td>
                                                <td style="width: 10%">{{$exams->side}}</td>
                                                <td style="width: 35%">{{$exams->customer_voice}}</td>
                                                <td>{{$exams->related}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                         </div>
                       @else  
                       @foreach ($exam as $exams)
                       <div class="card mb-3" align="left">
                                    <div class="card-header" align="center">
                                    <strong><h5>เฉลยคำตอบข้อที่ {{$exams->id - 5}}</h5></strong>
                                    </div>
                                    <div class="card-body"> 
                                        <img src="{{$exams->result}}" class="img-fluid" alt="Responsive image">
                                    </div>
                                </div>
            
                                <div class="card mb-3" align="center">
                                        <div class="card-header" >
                                                    <strong><h5>คำตอบของคุณ</h5></strong>
                                        </div>
                                        <div class="card-body"> 
                                            <h2>{{$exams->source}}</h2>
                                        </div>
                                    </div>
                             @endforeach
                       @endif
                </div>
                
            </div>
        </div>
</div>
@endsection
