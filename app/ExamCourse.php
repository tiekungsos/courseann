<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamCourse extends Model
{
    protected $table = 'exam_course';

    public function User()
    {
        return $this->belongsToMany('App\User' , 'exam_users', 'exam_course_id','users_id')->withPivot('source', 'sales' ,'side','customer_voice','related');
    }
}
