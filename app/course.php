<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course extends Model
{
    protected $table = 'course';

    protected $fillable = [
        'id','name', 'desc', 'created_at',  'updated_at'
    ];

    public function user(){
        return $this->belongsToMany('App\User')->withPivot('score');
    }

    public function datail()
    {
        return $this->hasMany('App\Detail');
    }

    public function examUser()
    {
        return $this->hasOne('App\ExamCourse');
    }
}
