<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'detail';

    protected $fillable = [
        'img', 'page', 'course_id'
    ];

    public function course()
    {
        return $this->belongsTo('App\course');
    }

    public function user(){
        return $this->belongsToMany('App\User')->withPivot('status');
    }
}
