<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Detail;
use App\UserDetail;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\course;
class DetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($couser,$page){
        $courseDetail = Detail::with(['course'])->where('course_id',$couser)->where('page',$page)->first();
        // dd($courseDetail);
        if(!$courseDetail){
            return abort(404,'Page not found');
        }
        // dd($courseDetail);
        $this->saveDetailUser($courseDetail->id,$couser,$page);

        $currentURL = url('/');
        $backToCourse = $currentURL.'/course/'.$this->backToCourse($couser);
        $imgurl = $currentURL.$courseDetail->img.'.jpg';
        $courseDetail->img = $imgurl;

        $percent  = 'width:'.(($courseDetail->page*100)/$courseDetail->course->page_full ).'%';
        $nextpage = $currentURL.'/detail'.'/'.$courseDetail->course_id.'/'.($courseDetail->page + 1);
        $previous =  $currentURL.'/detail'.'/'.$courseDetail->course_id.'/'.($courseDetail->page - 1);

        $courseBox = app('App\Http\Controllers\CourseController')->courseBox();

        return view('detail',compact('courseDetail','percent','nextpage','previous','backToCourse','courseBox'));
    }

    public function saveDetailUser($course,$couser,$page){
        $user = Auth::user();
        // dd($user);
        $checkCourse = UserDetail::where('detail_id',$page)->where('user_id',$user->id)->where('course_id',$couser)->first();

       
        if($checkCourse === null){
            DB::table('user_detail')->insert(['course_id' => $couser,'user_id' => $user->id, 'detail_id' => $course, 'status' => true]);
        }
       
    }

    private function backToCourse($couser){
        $head = course::select('child')->find($couser);
        return $head->child;
    }


}
