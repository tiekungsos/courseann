<?php

namespace App\Http\Controllers;
use App\course;
use App\UserDetail;
use App\Detail;
use App\ExamCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Collection;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $course = course::with(['user'])->where('child',null)->get();
        $currentURL = url()->current();
        $i = 0 ;


        foreach ($course as $courses) {
            $imgurl[$i] = $currentURL.$courses->img.'.jpg';
            $courses->img = $imgurl[$i];
            $detailCurrent = $this->currentPage($courses->id);
            $learnPage = $currentURL.'/course/'.$courses->id;
            $courses->url = $learnPage; 
            $i++;
        }

        $courseBox = $this->courseBox();
        return view('home',compact('course','courseBox'));
    }


    public function courseBox(){
        $course = course::where('child',null)->get()->toArray();
        $data = [];
        foreach ($course as $key => $value) {
           $data[$key] = $value;
                $datachild = course::where('child',$value['id'])->get()->toArray();

                foreach ($datachild as $keyin => $valuein) {
                    $data[$key]['child'][$keyin] =  $valuein;

                        if($valuein['child_in']){
                            $datachildin = course::where('child',$valuein['id'])->get()->toArray();
                            foreach ($datachildin as $keyinSec => $valueinSec) {
                                $data[$key]['child'][$keyin]['childin'][$keyinSec]  =  $valueinSec;

                                if($valueinSec['child_in']){
                                    $datachildinTree = course::where('child',$valueinSec['id'])->get()->toArray();
                                    foreach ($datachildinTree as $keyintree => $valueinTree) {
                                        $data[$key]['child'][$keyin]['childin'][$keyinSec]['childinin'][$keyintree]  =  $valueinTree;
                            }
                        }

                    }
                }
            }
        }
        return $data;
    }

    public function course($id){
        $course = course::with('user')->where('child',$id)->get();
        $currentURL = url('/');
        $i = 0 ;
        if($this->backToCourse($id)){
            $backToCourse = $currentURL.'/course/'.$this->backToCourse($id);
        }else {
            $backToCourse = $currentURL;
        }
        // $backToCourse = $currentURL.'/course/'.$this->backToCourse($id);

        $pagination = $this->pagination($id);

        foreach ($course as $courses) {
            $courses->testShow = false;
            $imgurl[$i] = $currentURL.$courses->img.'.jpg';
            $courses->img = $imgurl[$i];

            if($courses->child_in){
                $learnPage = $currentURL.'/course/'.$courses->id;
                $getCourseChild = course::select('id','page_full')->where('child',$courses->id)->get();
                $courses->testShow = $this->setTestCourseChild($getCourseChild);
                $pagecount = count($getCourseChild);
                $courses->currentPage = $pagecount;
            }else{
                $detailCurrent = $this->currentPage($courses->id);
                $learnPage = $currentURL.'/detail'.'/'.$courses->id.'/'.$detailCurrent;
                
                if($detailCurrent == 1){
                    $detailCurrent = 0;
                }
                $courses->currentPage = number_format(($detailCurrent*100)/$courses->page_full,0);
                
                if($courses->currentPage == 100){
                    $courses->testShow = true;
                }
            }

            if($courses->test){
                $exam = $this->seachExam($courses->id);
                $check = ExamCourse::where('course_id',$courses->id)->orderby('id','desc')->limit(1)->get();
                
                foreach ($check as $value) {
                    if($value->type == 2 ){
                        $exam = $exam + 5;
                    }

                    $courses->Testurl = $currentURL.'/exam'.'/'.$courses->id.'/'.$value->type.'/'.$exam;
                    
                    if($exam == $value->id + 1){
                        // $courses->Testurl = null;
                        $courses->Test = null;
                    }
                }
            }
            

            $courses->url = $learnPage; 
            $courses->currentString = 'width:'.$courses->currentPage.'%';
            $i++;

        }
        $courseBox = $this->courseBox();
        return view('course',compact('course','backToCourse','pagination','courseBox'));
    }
    
    //set current page exam 
    public function seachExam($id){
        $example = ExamCourse::where('course_id',$id)->orderBy('id', 'desc')->get();
        foreach ($example as $value) {
            $test = ExamCourse::find($value->id)
            ->User()
            ->wherePivot('users_id',Auth::user()->id)
            ->get();

            if($test->isNotEmpty()){
                foreach ($test as $valueIn) {
                    return $valueIn->pivot->exam_course_id + 1;
                }
            }else{
                return 1;
            }

        }
    } 

    private function setTestCourseChild($course){
        foreach ($course as $value) {
            $check = false;
            $courseDetail = $this->currentPage($value->id);
            $courseLearn = number_format(($courseDetail*100)/$value->page_full,0);
            if($courseLearn == 100){
                $check =  true;
            }
        }
        return $check;
    }

    private function currentPage($courseId){
        $user = Auth::user();
        $pageCurrent = null;
        $pageCurrent =  DB::table('user_detail')
        ->select('detail_id')
        ->where('user_id',$user->id)
        ->where('course_id',$courseId)
        ->orderBy('detail_id', 'desc')
        ->limit(1)
        ->get();

        
        if(count($pageCurrent) == 0){
           return  $pageCurrent = 1;
        }

        foreach ($pageCurrent as $page) {
            $pageIn = $page->detail_id;
            $pageThis =  Detail::find($page->detail_id);
            return $pageThis->page;
        }
        
        
    }

    private function backToCourse($couser){
        $head = course::select('child')->find($couser);
        return $head->child;
    }

    private function pagination($id){
        $couse = course::find($id);
        $collection = collect($couse->name);
        
        if($couse->child != null){
            $couse_in = course::find($couse->child);
            $collection->prepend($couse_in->name);
            // dd($couse_in);
            if($couse_in->child != null){
                $couse_in_next = course::find($couse_in->child);
                $collection->prepend($couse_in_next->name);
            }

        }
        $collection->all();
        return $collection;
    }
}
