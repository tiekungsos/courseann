<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamCourse;
use App\ExamUser;
use App\course;
use Illuminate\Support\Facades\Auth;
class ExamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($course,$type,$page){
        $currentURL = url('/');
        
        $exam = ExamCourse::where('course_id',$course)
        ->where('type',$type)
        ->where('id',$page)
        ->get();
        $page++;
        $backToCourse = $currentURL.'/course/'.$this->backToCourse($course);

        $courseBox = app('App\Http\Controllers\CourseController')->courseBox();
        if($exam->isNotEmpty()){
            $exam->fine = true;
            $examTest = $this->seachExam($course);
            foreach ($exam as $value) {

                if($value->type == 2){
                    $value->img =  $currentURL.$value->desc.'.jpg'; 
                }

                if($examTest == $value->id){
                    $exam->fine = false;
                }
            }
            return view('exam',compact('exam','backToCourse','courseBox'));
        }else{
            $exam = ExamCourse::where('course_id',$course)->get();
            
            foreach ($exam as $key=>$value) {
                $test = ExamCourse::find($value->id)
                ->User()
                ->wherePivot('users_id',Auth::user()->id)
                ->get()
                ->toArray();
                if($type == 1){
                    foreach ($test as $testIn) 
                    {
                            $value->source = $testIn['pivot']['source'];
                            $value->sales = $testIn['pivot']['sales'];
                            $value->side = $testIn['pivot']['side'];
                            $value->customer_voice = $testIn['pivot']['customer_voice'];
                            $value->related = $testIn['pivot']['related'];
                    }  
                }else{
                    $i = 1;
                    foreach ($test as $testIn) 
                    {
                            $value->result = $currentURL.'/images/customer_ser3_test/result/'.$i.'.jpg';
                            $value->source = $testIn['pivot']['source'];
                            $i++;
                    }  
                }
            }
            $image = '';
            if($type == 1){
                $image = '/images/workshop1.jpg';
            }

            return view('answer',compact('exam','backToCourse','image','courseBox','type'));
        }   
    }

    //set current page exam 
    public function seachExam($id){
        $example = ExamCourse::where('course_id',$id)->orderBy('id', 'desc')->get();
        foreach ($example as $value) {
            $test = ExamCourse::find($value->id)
            ->User()
            ->wherePivot('users_id',Auth::user()->id)
            ->get();

            if($test->isNotEmpty()){
                foreach ($test as $valueIn) {
                    return $valueIn->pivot->exam_course_id + 1;
                }
            }
        }
    } 

    private function backToCourse($couser){
        $head = course::select('child')->find($couser);
        return $head->child;
    }


    public function saveExam(Request $request,$course){
        
        // $request->validate([
        //     'source'=>'required',
        //     'sales'=> 'required',
        //     'side' => 'required',
        //     'customer_voice' => 'required',
        //     'related'=>'required'
        //   ]);


        $user = Auth::user();
        $ExamSave = new ExamUser($request->all());
        $ExamSave->exam_course_id =  $course;
        $ExamSave->users_id = $user->id;
        $ExamSave->timestamps = false;
        $ExamSave->save();

        $exam = ExamCourse::find($course);
        $course++;
        $nextCourse = '/exam'.'/'.$exam->course_id.'/'.$exam->type.'/'.$course;
        return redirect($nextCourse)->with('success', 'Next Page');
    }
}
