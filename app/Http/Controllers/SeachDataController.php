<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use App\course;

class SeachDataController extends Controller
{
    public function index($id)
    {
        $course = course::with(['datail'])->where('id',$id)->get();

        $courseBox = app('App\Http\Controllers\CourseController')->courseBox();
        $currentURL = url('/');
        foreach ($course as $value) {
            foreach ($value->datail as $valuein) {
                $imgurl = $currentURL.$valuein->img.'.jpg';
                $valuein->img = $imgurl;
            }
        }
        
        

        // dd($course);
        return view('seachdata',compact('course','courseBox'));
    }



}
