<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\course;
use App\Detail;
use App\UserDetail;
use App\ExamCourse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('delete',0)->latest()->get();
        return view('user.index',compact('user'));
           
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'emp_code' => ['required'],
            'username' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'nickname' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6'],
            'type' => ['required'],
        ]);

  
        User::create([
            'emp_code' => $request['emp_code'],
            'username' => $request['username'],
            'name' => $request['name'],
            'nickname' => $request['nickname'],
            'type' => $request['type'],
            'password' => Hash::make($request['password']),
        ]);
   
        return redirect()->route('user.index')
                        ->with('success','เพิ่มข้อมูลของ '. $request['name'] .' เรียบร้อยแล้ว');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        $course = course::where('child_in','=',null)->get();

        foreach ($course as $courses) {
            $detailCurrent = $this->currentPage($courses->id,$user->id);

            if($detailCurrent == 1){
                $detailCurrent = 0;
            }

            $courses->currentPage = number_format(($detailCurrent*100)/$courses->page_full,0);
            $courses->currentString = 'width:'.$courses->currentPage.'%';
            $courses->examType = null;

            if($courses->child == 8){
                $courses->exam =  $this->example($courses->child,$user->id);
                $courses->examType = 2;
            }else{
                $courses->exam =  $this->example($courses->id,$user->id);
            }

            
            $type = ExamCourse::select('type')->where('course_id',$courses->id)->limit(1)->get()->toArray();
            foreach ($type as $value) {
                $courses->examType = $value['type'];
            }
        }

        return view('user.show',compact('user','course'));
    }
   

    private function example($course,$user){
        $exam = ExamCourse::where('course_id',$course)->get();
        $currentURL = url('/');
        foreach ($exam as $key=>$value) {
            $test = ExamCourse::find($value->id)
            ->User()
            ->wherePivot('users_id',$user)
            ->get()
            ->toArray();

            if($value->type == 1){
                foreach ($test as $testIn) 
                {
                        $value->source = $testIn['pivot']['source'];
                        $value->sales = $testIn['pivot']['sales'];
                        $value->side = $testIn['pivot']['side'];
                        $value->customer_voice = $testIn['pivot']['customer_voice'];
                        $value->related = $testIn['pivot']['related'];
                }
                $value->image = '/images/workshop1.jpg';
            }else{
                $i = 1;
                foreach ($test as $testIn) 
                {
                        $value->result = $currentURL.'/images/customer_ser3_test/result/'.$i.'.jpg';
                        $value->source = $testIn['pivot']['source'];
                        $i++;
                }  
            }
        }
        return $exam;
    }


    private function currentPage($courseId,$userId){
        $pageCurrent = null;
        $pageCurrent =  UserDetail::select('detail_id')
        ->where('user_id',$userId)
        ->where('course_id',$courseId)
        ->orderBy('detail_id', 'desc')
        ->limit(1)
        ->get();

        if(count($pageCurrent) == 0){
           return  $pageCurrent = 1;
        }

        foreach ($pageCurrent as $page) {
            $pageIn = $page->detail_id;
            $pageThis =  Detail::find($page->detail_id);
            return $pageThis->page;
        }
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit',compact('user'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            // 'emp_code' => ['required', 'number'],
            'username' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'nickname' => ['required', 'string'],
            // 'password' => ['required', 'string', 'min:6'],
            // 'type' => ['required'],
        ]);
  
        if($request['password']){
            $user->username = $request['username'];
            $user->name = $request['name'];
            $user->nickname = $request['nickname'];
            // $user->type = $request['type'];
            $user->password = Hash::make($request['password']);
           
        }else{
            $user->username = $request['username'];
            $user->name = $request['name'];
            // $user->type = $request['type'];
            $user->nickname = $request['nickname'];
        }
        
        $user->save();
        $link = 'home';

        if(auth::user()->type == 1){
            $link = 'user.index';
        }



        return redirect()->route($link)
                        ->with('success','แก้ไขข้อมูลของ '. $user->name .' เรียบร้อยแล้ว');

    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // $user->delete();
        $user->delete = 1;
        $user->save();

        return redirect()->route('user.index')
                        ->with('success','User deleted successfully');
    }

}
