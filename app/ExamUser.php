<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamUser extends Model
{
    protected $table = 'exam_users';

    protected $fillable = [
        'exam_course_id','users_id', 'source', 'sales',  'side','customer_voice','related'
    ];
}
